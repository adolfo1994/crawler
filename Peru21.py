import re
import datetime
from bs4 import BeautifulSoup
from RPP import stripHtmlTags
from pprint import pprint


class Peru21(object):
    obtained_news = []

    def __init__(self, seed):
        self.url = seed
        self.domain = 'http://peru21.pe'
        self.name = "Peru21"

    def get_urls(self, page):
        soup = BeautifulSoup(page.text)
        urls = []
        for e in soup.find_all('a',
                               href=re.compile(
                                r'^/mundo/'
                                )):
            urls.append(self.domain + e.attrs['href'].split('?')[0])        
        return urls

    def extract_info(self, page):
        text = page.text
        soup = BeautifulSoup(text)
        tags = []
        tag_location = soup.find('div', 'nota-tags')
        date_tag = soup.find('div', 'fecha-nota')
        content_tag = soup.find('div', "nota-detalle")
        title_tag = soup.find('a', itemprop=re.compile('url'))
        print title_tag.string
        return

        if tag_location is None or date_tag is None or content_tag is None:
            return False
        print "Extracting from : ", page.url

        d = date_tag.attrs["datetime"][:10].split("-")
        date = datetime.date(int(d[0]), int(d[1]), int(d[2]))

        for t in tag_location.find_all('a'):
            if t.string is not None:
                tags.append(t.string.title())

        data = {}
        data['categoria'] = "Internacional"
        data['fecha'] = date.strftime('%d/%m/%Y')
        data['noticia'] = stripHtmlTags(content_tag.decode())
        data['url'] = page.url
        data['tag'] = tags

        self.obtained_news.append(data)

        return True
