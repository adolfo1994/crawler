import re
import datetime
from bs4 import BeautifulSoup
from RPP import stripHtmlTags


class Comercio(object):
    obtained_news = []

    def __init__(self, seed):
        self.url = seed
        self.domain = 'http://elcomercio.pe/mundo/'
        self.name = "Comercio"

    def get_urls(self, page):
        soup = BeautifulSoup(page.text)
        urls = []
        for e in soup.find_all('a',
                               href=re.compile(
                                r'^http://elcomercio.pe/mundo/'
                                )):
            urls.append(e.attrs['href'].split('?')[0])

        return urls

    def extract_info(self, page):
        text = page.text
        soup = BeautifulSoup(text)
        tags = []
        tag_location = soup.find('div', 'nota-tags')
        date_tag = soup.find('time')
        content_tag = soup.find('div', "nota-texto")
        if tag_location is None or date_tag is None or content_tag is None:
            return False
        for t in tag_location.find_all('a'):
            if t.string is not None:
                tags.append(t.string.title())
        print "Extracting from : ", page.url
        date = datetime.datetime.strptime(
            date_tag.find('meta').attrs['content'], "%Y-%m-%d").date()
        data = {}
        data['Categoria'] = "Internacional"
        data['Titulo'] = soup.find('h1',
                                   itemprop=re.compile('headline')).contents[0]
        data['Fecha'] = date.strftime('%d/%m/%Y')
        data['Noticia'] = stripHtmlTags(content_tag.decode())
        data['URL'] = page.url
        data['Tags'] = tags
        self.obtained_news.append(data)

        return True
