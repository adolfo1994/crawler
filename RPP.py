import re
import datetime
from bs4 import BeautifulSoup


def stripHtmlTags(htmlTxt):
    if htmlTxt is None:
        return None
    else:
        return ''.join(BeautifulSoup(htmlTxt).findAll(text=True))


class RPP(object):

    obtained_news = []

    def __init__(self, seed):
        self.url = seed
        self.domain = 'http://www.rpp.com.pe/'
        self.name = "RPP"

    def get_urls(self, page):
        soup = BeautifulSoup(page.text)
        urls = []
        for e in soup.find_all('a', href=re.compile('2015')):
            if e.attrs['href'][:2] == '20':
                urls.append(self.domain + e.attrs['href'])
        return urls

    def is_relevant(self, page):
        text = page.text
        soup = BeautifulSoup(text)
        is_relevant = False
        for t in soup.find_all('a', 'btn_tags'):
            if "Inter" in t.string:
                is_relevant = True
        return is_relevant

    def extract_info(self, page):
        url = page.url.split('/')[3]
        text = page.text
        soup = BeautifulSoup(text)
        tags = []
        is_relevant = False
        for t in soup.find_all('a', 'btn_tags'):
            tags.append(t.string.title())
            if "Inter" in t.string:
                is_relevant = True
        if not is_relevant:
            return False
        print "Extracting from : ", page.url
        date = datetime.date(
            int(url.split('-')[0]),
            int(url.split('-')[1]),
            int(url.split('-')[2])
            )
        data = {}
        content_tag = soup.find('div', "box_width-560")
        data['Categoria'] = "Internacional"
        data['Titulo'] = soup.find('h1', 'txt_t32-gris').contents[0]
        data['Fecha'] = date.strftime('%d/%m/%Y')
        data['Noticia'] = stripHtmlTags(content_tag.find_all('p')[3].decode())
        data['URL'] = page.url
        data['Tags'] = tags
        self.obtained_news.append(data)

        return True
