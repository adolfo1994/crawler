import re
import datetime
from selenium import webdriver
from bs4 import BeautifulSoup
from RPP import stripHtmlTags

months = {
    'ene': 1,
    'feb': 2,
    'mar': 3,
    'abr': 4,
    'may': 5,
    'jun': 6,
    'jul': 7,
    'ago': 8,
    'sep': 9,
    'oct': 10,
    'nov': 11,
    'dic': 12
}

class Terra(object):
    obtained_news = []

    def __init__(self, seed):
        self.url = seed
        self.domain = 'http://noticias.terra.com.pe/'
        self.name = "Terra"

    def get_urls(self, page):
        soup = BeautifulSoup(page.text)
        urls = []
        for e in soup.find_all('a',
                               href=re.compile(
                                r'^http://noticias.terra.com.pe/mundo'
                                )):
            urls.append(e.attrs['href'].split('?')[0])        
        return urls

    def extract_info(self, page):
        text = page.text
        soup = BeautifulSoup(text)
        tags = []
        tag_location = soup.find('div', 'channel')
        date_tag = soup.find('span', 'day-month')
        year_tag = soup.find('span', 'year')
        content_tag = soup.find('div', "articleData")
        if tag_location is None or date_tag is None or content_tag is None:
            return False
        for t in tag_location.find_all('a'):
            if t.string is not None:
                tags.append(t.string.title())
        print "Extracting from : ", page.url
        tags = [tag_location.contents[1].string]
        date = datetime.date(
            int(year_tag.string),
            months.get(date_tag.string.split(" ")[1], 1),
            int(date_tag.string.split(" ")[0])
            )
        content = ""
        for p in content_tag.find_all('p'):
            content += p.string

        data = {}
        data['Categoria'] = "Internacional"
        data['Titulo'] = soup.find('div',
                                   itemprop=re.compile('headline')).contents[1].string
        data['Fecha'] = date.strftime('%d/%m/%Y')
        data['Noticia'] = stripHtmlTags(content)
        data['URL'] = page.url
        data['Tags'] = tags
        self.obtained_news.append(data)

        return True
