# coding=utf8
import json
from pprint import pprint
import codecs
from django.utils.encoding import *


def main():
    f1 = open("outputs/finals/Comercio.json")
    f2 = open("outputs/finals/RPP.json")
    f3 = open("outputs/finals/Peru21.json")
    com = json.load(f1)
    rpp = json.load(f2)
    p21 = json.load(f3)
    com = com + rpp
    new_json = []
    for x in com:
        n_data = {}
        n_data['categoria'] = x['Categoria']
        n_data['titulo'] = smart_text(x['Titulo']).encode('utf8')
        n_data['fecha'] = x['Fecha']
        n_data['noticia'] = smart_text(x['Noticia']).encode('utf8')
        n_data['url'] = x['URL']
        n_data['tag'] = x['Tags']
        new_json.append(n_data)

    new_json += p21
    f1.close()
    f2.close()
    f3.close()
    with codecs.open("outputs/Internacional.json", "w+", "utf8") as f:
        json.dump(new_json, f, indent=4, ensure_ascii=False, encoding="utf8")

if __name__ == '__main__':
    main()
