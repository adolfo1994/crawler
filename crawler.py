import requests
import json
import os
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Crawler(object):
    visited = set()

    def __init__(self, md, seeds):
        self.max_depth = md
        self.seeds = seeds

    def crawl_all(self):
        for seed in self.seeds:
            urls = [x for x in seed.url]
            while urls:
                if len(seed.obtained_news) < 10:
                    next_url = urls.pop(0)
                    if next_url not in self.visited:
                        resp = requests.get(next_url)
                        for u in seed.get_urls(resp):
                            if u not in self.visited:
                                urls.append(u)
                        self.visited.add(next_url)
                        if seed.extract_info(resp):
                            logger.info("URLS left " + str(len(urls)))
                            logger.info("News obtained" + str(
                                len(seed.obtained_news)))
                else:
                    previous = []
                    with open("outputs/"+seed.name+".json", "r+") as output:
                        previous = json.load(output)
                        previous += seed.obtained_news
                        logger.info("Total news until now: " + str(
                                len(previous)))
                        seed.obtained_news = []

                    with open("outputs/"+seed.name+".json", "w+") as output:
                        json.dump(previous, output, indent=4)
                        statinfo = os.stat("outputs/"+seed.name+".json")
                        logger.info("Outputted file of size: %i" %
                                    statinfo.st_size)
        return
